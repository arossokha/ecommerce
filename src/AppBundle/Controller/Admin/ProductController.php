<?php

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductController
 *
 * @Route("/admin/product")
 */
class ProductController extends Controller
{
    /**
     * @param Request $request
     * @return array
     * @Route(".html",name="products_list",methods={"get"})
     * @Template("AppBundle:admin/product:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $doctrine = $this->get('doctrine');
        $products = $doctrine->getRepository('AppBundle:Product')->findAllOrderedByUpdated();
        return [
            'products' => $products
        ];
    }

    /**
     * @param Request $request
     * @return array
     * @Route("/create.html",name="product_create",methods={"get","post"})
     * @Template("AppBundle:admin:product/create.html.twig")
     */
    public function createAction(Request $request)
    {
        $form = $this->createProductForm();
        $form->handleRequest($request);
        if($request->isMethod(Request::METHOD_POST) && $form->isValid()) {
            $doctrine = $this->get('doctrine');
            $em = $doctrine->getManager();
            $em->persist($form->getData());
            $em->flush();
            $this->get('session')->getFlashBag()->add('success','Product created successfully');
            return new RedirectResponse($this->generateUrl('products_list'));
        }

        return [
            'form' => $form->createView()
        ];
    }

    protected function createProductForm(Product $entity = null) {
        if(is_null($entity)) {
            $entity = new Product();
        }
        $form = $this->createForm(new ProductType(),$entity);
        $form->add('submit','submit');
        return $form;
    }

    /**
     * @param Request $request
     * @return array
     * @Route("/update/{id}.html",name="product_update",requirements={"id":"\d+"},methods={"get","post"})
     * @Template("AppBundle:admin:product/update.html.twig")
     */
    public function updateAction(Request $request, Product $product)
    {
        $form = $this->createProductForm($product);
        $form->handleRequest($request);
        if($request->isMethod(Request::METHOD_POST) && $form->isValid()) {
            $doctrine = $this->get('doctrine');
            $em = $doctrine->getManager();
            $em->persist($form->getData());
            $em->flush();
            $this->get('session')->getFlashBag()->add('success','Product updated successfully');
            return new RedirectResponse($this->generateUrl('products_list'));
        }

        return [
            'form' => $form->createView()
        ];
    }
}