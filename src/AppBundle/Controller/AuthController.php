<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AuthController
 * @package AppBundle\Controller
 * @Route("/auth")
 */
class AuthController extends Controller
{
    /**
     * @return array
     * @Route("/login",name="login")
     * @Template("AppBundle:auth:login.html.twig")
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return [
            'last_username' => $lastUsername,
            'error'         => $error,
        ];
    }

    /**
     * @Route("/login_check",name="login_check")
     */
    public function loginCheckAction()
    {

    }

    /**
     * @Route("/logout",name="logout")
     */
    public function logoutAction()
    {

    }
}