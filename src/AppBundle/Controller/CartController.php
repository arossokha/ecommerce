<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Cart;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class CartController
 * @package AppBundle\Controller
 * @Route("/cart")
 */
class CartController extends Controller
{
    /**
     * @return array
     * @Route(".html",name="cart_index")
     * @Template("AppBundle:cart:index.html.twig")
     */
    public function indexAction()
    {
        $cartService = $this->get('app.cart_service');
        $entity = $cartService->getCart();

        return [
            'cart' => $entity
        ];
    }
}