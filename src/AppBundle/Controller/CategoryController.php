<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CategoryController
 *
 * @Route("/category")
 */
class CategoryController extends Controller
{
    const DEFAULT_LIMIT = 20;
    /**
     * @Route("/{id}.html", name="show_category",requirements={"id":"\d+"},methods={"get"})
     */
    public function indexAction($id)
    {
        $doctrine = $this->container->get('doctrine');
        $repository = $doctrine->getRepository('AppBundle:Category');
        try {
            $entity = $repository->findExistingEntity($id);
        } catch (EntityNotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage());
        }
        $products = $entity->getProducts();
        return $this->render('AppBundle:category:index.html.twig',[
            'category' => $entity,
            'products' => $products,
        ]);
    }

    /**
     * @Route("/{id}/show.html", name="show_category_one",requirements={"id":"\d+"},methods={"get"})
     */
    public function showAction(Category $category)
    {
        dump($category);

        return new Response();
    }

    /**
     * @Route("/{id}/products.html", name="show_category_products",requirements={"id":"\d+"},methods={"get"})
     * @Template("AppBundle:category:index.html.twig")
     */
    public function productsAction(Request $request, $id)
    {
        $doctrine = $this->container->get('doctrine');
        $categoryRepository = $doctrine->getRepository('AppBundle:Category');
        $productRepository = $doctrine->getRepository('AppBundle:Product');
        $limit = $request->get('limit', self::DEFAULT_LIMIT);
        $offset = $request->get('offset', 0);
        try {
            $entity = $categoryRepository->findExistingEntity($id);
            $entities = $productRepository->findActiveProductsByCategory($entity,$limit,$offset);
        } catch (EntityNotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage());
        }

        return [
            'category' => $entity,
            'products' => $entities

        ];
    }
}
