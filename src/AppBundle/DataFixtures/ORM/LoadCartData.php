<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Cart;
use AppBundle\Entity\CartItem;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadCartData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadCartData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const CSV_DELIMITER = ';';
    const CSV_LINE_LIMIT = 100000;
    const EMAIL_COLUMN = 0;
    const TOTAL_COLUMN = 1;
    const PRODUCTS_COLUMN = 2;

    protected $dataFileAlias = '@AppBundle/Resources/data/cart.csv';

    /**
     * @return string
     */
    public function getDataFileAlias()
    {
        return $this->dataFileAlias;
    }

    /**
     * @todo: refactor this code create main class for fixtures
     * @param ObjectManager $manager
     * @return bool
     */
    public function load(ObjectManager $manager)
    {
        /**
         * @todo: get file name from kernel
         */
        $fileLocator = $this->container->get('file_locator');
        $dataFileName = $fileLocator->locate($this->getDataFileAlias());

        if(!file_exists($dataFileName)) {
            return false;
        }
        $fp = fopen($dataFileName,'r');
        $columnNamesRow = fgetcsv($fp,self::CSV_LINE_LIMIT, self::CSV_DELIMITER);
        $passwordEncoder = $this->container->get('security.password_encoder');
        while(true) {
            $data = fgetcsv($fp,self::CSV_LINE_LIMIT, self::CSV_DELIMITER);
            if(false === $data) {
                break;
            }
            $userRepository = $manager->getRepository('AppBundle:User');
            if(count($data) === 3 ) {
                $user = $userRepository->findByEmail($data[self::EMAIL_COLUMN]);

                $productSkus = explode(',',$data[self::PRODUCTS_COLUMN]);
                if(count($user)) {
                    $cart = new Cart();
                    $cart->setTotal($data[self::TOTAL_COLUMN]);
                    $cart->setUser($user[0]);
                    foreach($productSkus as $sku) {
                        $refName = 'prod_'.$sku;
                        if($this->hasReference($refName)) {
                            /**
                             * @var $product Product
                             */
                            $product = $this->getReference($refName);

                            $cartItem = new CartItem();
                            $cartItem->setProduct($product);
                            $cartItem->setPrice($product->getPrice());
                            $cart->addItem($cartItem);
                            $manager->persist($cartItem);
                        }
                    }
                    $manager->persist($cart);
                }
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 155;
    }
}