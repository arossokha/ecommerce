<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadCategoryData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const CSV_DELIMITER = ';';
    const CSV_LINE_LIMIT = 100000;
    const TITLE_COLUMN = 0;
    const ACTIVE_COLUMN = 1;
    const REFERENCE_COLUMN = 2;

    protected $dataFileAlias = '@AppBundle/Resources/data/category.csv';

    /**
     * @return string
     */
    public function getDataFileAlias()
    {
        return $this->dataFileAlias;
    }

    /**
     * @todo: refactor this code create main class for fixtures
     * @param ObjectManager $manager
     * @return bool
     */
    public function load(ObjectManager $manager)
    {
        /**
         * @todo: get file name from kernel
         */
        $fileLocator = $this->container->get('file_locator');
        $dataFileName = $fileLocator->locate($this->getDataFileAlias());

        if(!file_exists($dataFileName)) {
            return false;
        }
        $fp = fopen($dataFileName,'r');
        $columnNamesRow = fgetcsv($fp,self::CSV_LINE_LIMIT, self::CSV_DELIMITER);

        while(true) {
            $data = fgetcsv($fp,self::CSV_LINE_LIMIT, self::CSV_DELIMITER);
            if(false === $data) {
                break;
            }
            if(count($data) === 3 ) {
                $category = (new Category())
                    ->setTitle($data[self::TITLE_COLUMN])
                ;
                if(1 === (int)$data[self::ACTIVE_COLUMN]) {
                    $category->enable();
                } else {
                    $category->disable();
                }
                $referenceName = trim($data[self::REFERENCE_COLUMN]);
                $this->setReference($referenceName,$category);
                $manager->persist($category);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 100;
    }

}