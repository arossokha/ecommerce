<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Product;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadProductData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadProductData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const CSV_DELIMITER = ';';
    const CSV_LINE_LIMIT = 100000;
    const TITLE_COLUMN = 0;
    const ACTIVE_COLUMN = 1;
    const SKU_COLUMN = 2;
    const PRICE_COLUMN = 3;
    const DESC_COLUMN = 4;
    const CATEGORY_COLUMN = 5;
    const IMAGE_COLUMN = 6;

    protected $dataFileAlias = '@AppBundle/Resources/data/product.csv';

    /**
     * @return string
     */
    public function getDataFileAlias()
    {
        return $this->dataFileAlias;
    }

    /**
     * @todo: refactor this code create main class for fixtures
     * @param ObjectManager $manager
     * @return bool
     */
    public function load(ObjectManager $manager)
    {
        /**
         * @todo: get file name from kernel
         */
        $fileLocator = $this->container->get('file_locator');
        $dataFileName = $fileLocator->locate($this->getDataFileAlias());

        if(!file_exists($dataFileName)) {
            return false;
        }
        $fp = fopen($dataFileName,'r');
        $columnNamesRow = fgetcsv($fp,self::CSV_LINE_LIMIT, self::CSV_DELIMITER);

        while(true) {
            $data = fgetcsv($fp,self::CSV_LINE_LIMIT, self::CSV_DELIMITER);
            if(false === $data) {
                break;
            }
            if(count($data) === 7 ) {
                $product = (new Product())
                    ->setSku($data[self::SKU_COLUMN])
                    ->setTitle($data[self::TITLE_COLUMN])
                    ->setDescription($data[self::DESC_COLUMN])
                    ->setImage($data[self::IMAGE_COLUMN])
                    ->setPrice((float)$data[self::PRICE_COLUMN])
                ;

                if(1 === (int)$data[self::ACTIVE_COLUMN]) {
                    $product->enable();
                } else {
                    $product->disable();
                }
                $referenceName = trim($data[self::CATEGORY_COLUMN]);
                if($this->hasReference($referenceName)) {
                    $product->setCategory($this->getReference($referenceName));
                }

                $this->setReference('prod_'.$product->getSku(),$product);
                $manager->persist($product);
            }
        }

        $manager->flush();

    }

    public function getOrder()
    {
        return 150;
    }

}