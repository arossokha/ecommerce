<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadUserData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const CSV_DELIMITER = ';';
    const CSV_LINE_LIMIT = 100000;
    const EMAIL_COLUMN = 0;
    const PASSWORD_COLUMN = 1;
    const ROLES_COLUMN = 2;

    protected $dataFileAlias = '@AppBundle/Resources/data/user.csv';

    /**
     * @return string
     */
    public function getDataFileAlias()
    {
        return $this->dataFileAlias;
    }

    /**
     * @todo: refactor this code create main class for fixtures
     * @param ObjectManager $manager
     * @return bool
     */
    public function load(ObjectManager $manager)
    {
        /**
         * @todo: get file name from kernel
         */
        $fileLocator = $this->container->get('file_locator');
        $dataFileName = $fileLocator->locate($this->getDataFileAlias());

        if(!file_exists($dataFileName)) {
            return false;
        }
        $fp = fopen($dataFileName,'r');
        $columnNamesRow = fgetcsv($fp,self::CSV_LINE_LIMIT, self::CSV_DELIMITER);
        $passwordEncoder = $this->container->get('security.password_encoder');
        while(true) {
            $data = fgetcsv($fp,self::CSV_LINE_LIMIT, self::CSV_DELIMITER);
            if(false === $data) {
                break;
            }
            if(count($data) === 3 ) {
                $user = (new User())
                    ->setEmail($data[self::EMAIL_COLUMN])
                    ->setRoles(explode(',',$data[self::ROLES_COLUMN]))
                ;
                $password = $passwordEncoder->encodePassword($user,$data[self::PASSWORD_COLUMN]);
                $user->setPassword($password);

                $manager->persist($user);
            }
        }

        $manager->flush();

    }

    public function getOrder()
    {
        return 150;
    }
}