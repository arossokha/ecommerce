<?php

namespace AppBundle\Entity;

interface ActiveAwareInterface {
    public function isActive();
}