<?php

namespace AppBundle\Entity;

interface ChangeControlAwareInterface {
    public function getUpdatedAt();
    public function getCreatedAt();
//    public function getUpdatedBy();
}