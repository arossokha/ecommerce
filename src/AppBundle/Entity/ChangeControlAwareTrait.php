<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

trait ChangeControlAwareTrait
{
    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updated_at;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $create_at;

    public function getUpdatedAt()
    {
        $this->updated_at;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersistChangeControl()
    {
        if (is_null($this->create_at)) {
            $this->create_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    public function getCreatedAt()
    {
        $this->create_at;
    }
//
//    /**
//     * @var User
//     *
//     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User",cascade={"persist"})
//     */
//    private $user;
//
//    public function getUpdatedBy()
//    {
//        return $this->user;
//    }
//
//    public function setUpdatedBy(User $user)
//    {
//        $this->setUpdatedBy($user);
//
//        return $this;
//    }
}