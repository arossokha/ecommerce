<?php
namespace AppBundle\Event;

use AppBundle\Entity\Cart;
use Symfony\Component\EventDispatcher\Event;

class AdditionalPaymentEvent extends Event
{
    /**
     * @var Cart
     */
    private $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }
}