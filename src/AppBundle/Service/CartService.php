<?php

namespace AppBundle\Service;

use AppBundle\Entity\Cart;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CartService
{
    use LoggerAwareTrait;
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function getCart()
    {
        $token = $this->tokenStorage->getToken();
        $em = $this->doctrine->getManager();
        if (is_null($token)) {
            throw new UnauthorizedHttpException();
        }
        /**
         * @var $user User
         */
        $user = $token->getUser();
        if (is_null($user->getCart())) {
            $cart = new Cart();
            $cart->setUser($user);
            $em->persist($cart);
            $this->logger->debug('Cart created for user '.$user->getEmail());
        }


        return $user->getCart();
    }

    public function addProduct(Product $product)
    {
        $cart = $this->getCart();
        $this->doctrine->persist($cart);
        $this->doctrine->flush();

        return $product;
    }
}